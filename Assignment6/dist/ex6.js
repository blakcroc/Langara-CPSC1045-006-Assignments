let baseString = "TestPage"
function populatePage() {
    let containers = document.querySelectorAll(".container"); //get all the containers

    //Part 1: Rewrite the following code using a for/of loop. 
    //Trivial example: This code capitalizes what is in the HTML file
    //and counts the number of charcters
    //reference for/of loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of
    //reference for loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for
    let count = 0;
    for(var i=0; i<containers.length; i++){
        count += containers[i].innerHTML.length;
        containers[i].innerHTML = containers[i].innerHTML.toUpperCase();
    }
    //Output Part 1: You do not need to change this.
    let output = document.querySelector("#output");
    output.innerHTML = "Number of characters:" + count;

    //Part 2: Rewrite the following using a loop and a function
    let heights = [4, 5, 3, 7, 6, 10,-2];
    let pointString = "";//start with an empy string
    let x = 0;

    //Replace the following code with a for/of loop.
    //In this loop, call a function that you will write to 
    //help create the string of points
    for(var i=0; i<heights.length; i++){
        x+= 10;
        y = 200 - heights[i] *20;
        pointString = MakeAString(pointString,x,y);
    }

    function MakeAString(pointString,x,y) {
        pointString = pointString + x + "," + y + " ";
        return String(pointString);
    }
    //Take the next line of code and make it a function
    //You function will take in, x, y, and the existing string ( 3 parameters into total)
    //and return the new string with the ponts added.
    //Reference: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions
   let output2 = document.querySelector("#output2");
    output2.setAttribute("points", pointString);


    //Part 3:  Creating things with loop
    // on the html page with loop.
    //mananually, there are other ways of course.
    let data = [30, 70, 110, 150];
    let svgString = '<svg width="500" height="500">';
    x= 0;

    for(var i=0; i<data.length; i++){
        x = data[i];
        circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
        svgString += circleString;
    }
    
    svgString += "</svg>";
    let output3 = document.querySelector("#output3");
    output3.innerHTML += svgString;

}




