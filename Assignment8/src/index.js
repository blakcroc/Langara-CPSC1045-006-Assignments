//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

function ChangeColor() {
  let cx = event.offsetX;
  let cy = event.offsetY;
  let color = document.querySelector("#color");
  let svgString = '<svg onclick="ChangeColor()" width="400" height="400" >';
  let circleString = '<circle cx="' + Number(cx) + '" cy="' + Number(cy) + '" r="36" stroke="black" fill="' + String(color.value) + '" />';
  svgString += circleString;
  svgString += '</svg>';
  let output = document.querySelector("#output");
    output.innerHTML = svgString;
  console.log(svgString);
  }