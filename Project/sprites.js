console.log("Sprite file is connected");

function drawAnimal(x,y) {
    let translateString = "translate(" + x + " " + y +")";
    let output = document.querySelector("#dog");
    let changeString = String(output.innerHTML);
    let newString = '<g id="dog" transform="' + translateString + '">' + changeString + ' </g>';
    return (newString);
}

function drawFruit(x,y) {
    let translateString = "translate(" + x + " " + y +")";
    let output = document.querySelector("#apple");
    let changeString = String(output.innerHTML);
    let newString = '<g id="apple" transform="' + translateString +'">' + changeString + ' </g>';
    return (newString);
}

function drawVehicle(x,y) {
    let translateString = "translate(" + x + " " + y +")";
    let output = document.querySelector("#car");
    let changeString = String(output.innerHTML);
    let newString = '<g id="car" transform="' + translateString + '">' + changeString + ' </g>';
    return (newString);
}

function drawPlant(x,y) {
    let translateString = "translate(" + x + " " + y +")";
    let output = document.querySelector("#plant");
    let changeString = String(output.innerHTML);
    let newString = '<g id="plant" transform="' + translateString + '">' + changeString + ' </g>';
    return (newString);
}

console.log(drawAnimal(5,10));
console.log(drawAnimal(100,105));
console.log(drawAnimal(265,310));

console.log(drawPlant(186,119));
console.log(drawPlant(59,188));
console.log(drawPlant(145,26));

console.log(drawFruit(145,171));
console.log(drawFruit(216,232));
console.log(drawFruit(180,149));

console.log(drawVehicle(192,224));
console.log(drawVehicle(158,42));
console.log(drawVehicle(61,223));