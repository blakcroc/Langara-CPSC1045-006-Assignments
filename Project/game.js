var gameNOTrunning = true;
var dropping;

var gameStart = document.getElementById('run');
var gameRestart = document.getElementById('restart');
gameRestart.style.visibility = 'hidden';

var originalString = document.querySelector("#game").innerHTML;

var empty ='<rect x="750" y="10.75" width="49.25" height="49.25" style="fill:white;stroke:white;stroke-width:1.5" />';
var block1 = `<rect id="uno" x="750" y="10.75" width="49.25" height="49.25" style="fill:yellow;stroke:black;stroke-width:1.5" />`;
var block2 = `<rect id="dos" x="750" y="10.75" width="49.25" height="49.25" style="fill:purple;stroke:black;stroke-width:1.5" />`;
var block3 = `<rect id="tres" x="750" y="10.75" width="49.25" height="49.25" style="fill:green;stroke:black;stroke-width:1.5" />`;



var display =   [[empty, empty, empty, empty, empty, empty, empty],             // the well is 7 columns x 8 rows + 1 row above the well
                [empty, empty, empty, empty, empty, empty, empty],
                [empty, empty, empty, empty, empty, empty, empty],
                [empty, empty, empty, empty, empty, empty, empty],
                [empty, empty, empty, empty, empty, empty, empty],
                [empty, empty, empty, empty, empty, empty, empty],
                [empty, empty, empty, empty, empty, empty, empty],
                [empty, empty, empty, empty, empty, empty, empty],
                [empty, empty, empty, empty, empty, empty, empty]];

var column = [750, 750, 750, 750, 750, 750, 750];
var columnNumber;
var rowNumber;
var WellSTR = '';

var count = 0;
var b;
var game;
var gameCopy;
var copy;

var yDropping;
var xChange;



var shape1 = `<g id="1" fill="yellow">
<rect x="750" y="10.75" width="49.25" height="49.25" style="stroke:black;stroke-width:1.5" />
</g>`;


var shape2 = `<g id="2" fill="purple">
<rect x="750" y="10.75" width="49.25" height="49.25" style="stroke:black;stroke-width:1.5" />
<rect x="800" y="10.75" width="49.25" height="49.25" style="stroke:black;stroke-width:1.5" />
<rect x="750" y="60.75" width="49.25" height="49.25" style="stroke:black;stroke-width:1.5" />
<rect x="800" y="60.75" width="49.25" height="49.25" style="stroke:black;stroke-width:1.5" />
</g>`;

var shape3 = `<g id="3" fill="green">
<rect x="750" y="10.75" width="49.25" height="49.25" style="stroke:black;stroke-width:1.5" />
<rect x="800" y="10.75" width="49.25" height="49.25" style="stroke:black;stroke-width:1.5" />
</g>`;

var shape4 = `<g id="4" fill="green">
<rect x="750" y="10.75" width="49.25" height="49.25" style="stroke:black;stroke-width:1.5" />
<rect x="750" y="60.75" width="49.25" height="49.25" style="stroke:black;stroke-width:1.5" />
</g>`;

var shapes = [shape1, shape2, shape3, shape4];

var $hape = 0;

console.log(display[1][3]);
function runGame(){
    gameStart.style.visibility = "hidden";
    gameRestart.style.visibility = 'visible';
    for(i=0; i<9; i++) {
        for (p=0; p<7; p++) {
            let l = (p * 50) + -100;
            let k = 700 - (i * 50); 
            WellSTR += ' <g transform="translate(' + l + ' ' + k + ')"> ' + display[i][p] + '</g> ';
        }
    }
    let StringToReplace = document.querySelector("#insideTheWell");
    StringToReplace.innerHTML = WellSTR;
    gameCopy = document.querySelector("#game");
    game = gameCopy.innerHTML;
    copy = game;
    b = Math.floor(3 * Math.random());
    $hape = b + 1;
    game += shapes[b];
    gameCopy.innerHTML = game;
    yDropping = 0;
    xChange = 0;
    if($hape == 2 || $hape == 4){
        rowNumber = 13;
    }
    else{
        rowNumber = 14 ;
    }
    columnNumber = 2;
    dropping = setInterval(Drop, 1337);
    gameNOTrunning = false;
}

function resetGame(){
    gameStart.style.visibility = "visible";
    gameRestart.style.visibility = 'hidden';
    column = [750, 750, 750, 750, 750, 750, 750];
    count = 0;
    let Score = document.getElementById("score");
    Score.innerHTML = count + '</text>';
    count = 0;
    xChange = 0;
    yDropping = 0;
    columnNumber = 2;
    WellSTR = '';
    display =   [[empty, empty, empty, empty, empty, empty, empty],             // the well is 7 columns x 8 rows + 1 row avove the well
                    [empty, empty, empty, empty, empty, empty, empty],
                    [empty, empty, empty, empty, empty, empty, empty],
                    [empty, empty, empty, empty, empty, empty, empty],
                    [empty, empty, empty, empty, empty, empty, empty],
                    [empty, empty, empty, empty, empty, empty, empty],
                    [empty, empty, empty, empty, empty, empty, empty],
                    [empty, empty, empty, empty, empty, empty, empty],
                    [empty, empty, empty, empty, empty, empty, empty]];

    let a = document.querySelector("#game");
    a.innerHTML = originalString;
    clearInterval(dropping);

}

function pauseGame(){
    if (gameStart.style.visibility === 'hidden'){
    clearInterval(dropping);
    gameNOTrunning = true;
    }
}

function resumeGame(){
    if (gameNOTrunning && gameStart.style.visibility === 'hidden') {
    dropping = setInterval(Drop, 1337);
    gameNOTrunning = false;
    }
}


function Drop() {
    yDropping += 50;
    rowNumber -= 1;
    let dropString = 'translate('+ xChange + ' ' + yDropping + ')';
    document.getElementById($hape).setAttribute("transform",dropString);
    console.log(yDropping);
    console.log(rowNumber);
    console.log(!CanDrop());
    if (!CanDrop()) {
        AddBlock();
    }
    if (tooLow()){
        clearInterval(dropping);
        yDropping = 0;
        xChange = 0;
        columnNumber = 2;
        b = Math.floor(3 * Math.random());
        gameCopy = document.querySelector("#game");
        $hape = b + 1;
        if($hape == 2 || $hape == 4){
            rowNumber = 13;
        }
        else{
            rowNumber = 14 ;
        }
        game = copy;
        game += shapes[b];
        gameCopy.innerHTML = game;
        dropping = setInterval(Drop, 1337);
    }
    }

function CanDrop() {
    if ($hape == 1){
        if(yDropping < 350){
            return(true);
        }
        if(yDropping >= 350 && (xChange >= 250 || xChange <= -150)){
            return(true);
        } 
        if(yDropping == 700 && (xChange >= -100 && xChange <= 200)){
            return (false);
        }   
        if(yDropping >= 350 && yDropping < 700 && (xChange >= -100 && xChange <= 200)){
            if(display[rowNumber - 1][columnNumber] != empty ){
                return(false);
            }
            else{
                return (true);
            }
        }
    }
    if ($hape == 2){
        if(yDropping < 300){
            return(true);
        }
        if(yDropping >= 300 && (xChange >= 250 || xChange <= -200)){
            return(true);
        }
        if(yDropping == 650 && (xChange >= -100 && xChange <= 150)){
            return (false);
        }  
        if(yDropping >= 300 && yDropping < 700 && (xChange >= -100 && xChange <= 150)){
            if(display[rowNumber - 1][columnNumber] != empty || display[rowNumber - 1][columnNumber + 1] != empty ){
                return(false);
            }
            else{
                return(true);
            }
        }
    }
    if ($hape == 3){
        if(yDropping < 350){
            return(true);
        }
        if(yDropping >= 350 && (xChange >= 250 || xChange <= -200)){
            return(true);
        }
        if(yDropping == 700 && (xChange >= -100 && xChange <= 150)){
            return (false);
        }  
        if(yDropping >= 350 && yDropping < 700 && (xChange >= -100 && xChange <= 150)){
            if(display[rowNumber - 1][columnNumber] != empty || display[rowNumber -1][columnNumber + 1] != empty ){
                return(false);
            }
            else{
                return(true);
            }
        }    
    }
    if ($hape == 4){
        if(yDropping < 300){
            return(true);
        }
        if(yDropping >= 300 && (xChange >= 250 || xChange <= -150)){
            return(true);
        }
        if(yDropping == 650 && (xChange >= -100 && xChange <= 200)){
            return (false);
        }  
        if(yDropping >= 300 && yDropping < 650 && (xChange >= -100 && xChange <= 200)){
            if(display[rowNumber - 1][columnNumber] != empty ){
                return(false);
            }
            else{
                return(true);
            }
        }
    }

} 

function tooLow() {
    if (yDropping === 750 ) {
        return(true);
    }
    else{
        return(false);
    }
}

function AddBlock() {
    console.log ('row: ' + rowNumber);
    gameCopy = document.querySelector("#game");
    gameCopy.innerHTML = copy;
    clearInterval(dropping);
    yDropping = 0;
    console.log (columnNumber);
    if($hape === 4) {
        display[rowNumber][columnNumber] = block3;
        display[rowNumber + 1][columnNumber] = block3;
    }
    if($hape === 2) {
        display[rowNumber][columnNumber] = block2;
        display[rowNumber][(columnNumber + 1)] = block2;
        display[rowNumber + 1][columnNumber] = block2;
        display[rowNumber + 1][(columnNumber + 1)] = block2;
    } 
    if($hape === 3) {
        display[rowNumber][columnNumber] = block3;
        display[rowNumber][(columnNumber + 1)] = block3;
    }    
    if($hape === 1){
        display[rowNumber][columnNumber] = block1;
    }
    console.log(display);
    let checkRowFull = 0;
    let cleaning = 0;
    for(i=0; i<9; i++){
        for (p=0;p<7;p++){
            if (display[i][p] != empty){
            checkRowFull += 1;
        }
    }
        if (checkRowFull == 7){
            for(u=i;u<8;u++){
               display[u] = display[u + 1]; 
            }
            cleaning += 1;
            i -= 1;
            display[8] = [empty, empty, empty, empty, empty, empty, empty];
        }
        checkRowFull = 0;
    }
    if (cleaning == 0){
        count += 0;
    }
    if (cleaning == 1){
        count += 1;
    }

    if (cleaning == 2){
        count += 10;
    }
    let Score = document.getElementById("score");
    Score.innerHTML = count + '</text>';
    console.log(display);
    WellSTR = '';
    for(i=0; i<9; i++) {
        for (p=0; p<7; p++) {
            let l = (p * 50) + -100;
            let k = 700 - (i * 50); 
            WellSTR += ' <g transform="translate(' + l + ' ' + k + ')"> ' + display[i][p] + '</g> ';
        }
    }
    let StringToReplace = document.querySelector("#insideTheWell");
    StringToReplace.innerHTML = WellSTR;
    gameCopy = document.querySelector("#game");
    copy = gameCopy.innerHTML;
    game = copy;
    xChange = 0;
    columnNumber = 2;
    b = Math.floor(3 * Math.random());
    $hape = b + 1;
    if($hape == 2 || $hape == 4){
        rowNumber = 13;
    }
    else{
        rowNumber = 14 ;
    }
    game += shapes[b];
    gameCopy.innerHTML = game;
    dropping = setInterval(Drop, 1337);
}

document.addEventListener('keydown', function(event) {
    keyEvent = event.keyCode;
    if(keyEvent === 37){
        if ($hape == 1){
            if (yDropping < 350){
                xChange -= 50;
            }
            if (yDropping >= 350 && (xChange <= -150 || xChange > 250)){
                xChange -= 50;
            }
            if (yDropping >= 350 && (xChange > -100 && xChange <= 200 )){
                if (display[rowNumber][columnNumber - 1] == empty){
                    xChange -= 50;
                }
            }      
        }
        if ($hape == 2){
            if (yDropping < 300){
                xChange -= 50;
            }
            if (yDropping >= 300 && (xChange <= -200 || xChange > 250)){
                xChange -= 50;
            }
            if (yDropping >= 300 && (xChange > -100 && xChange <= 150 )){
                if (display[rowNumber][columnNumber - 1] == empty && display[rowNumber + 1][columnNumber - 1] == empty ){
                    xChange -= 50;
                }
            }      
        }
        if ($hape == 3){
            if (yDropping < 350){
                xChange -= 50;
            }
            if (yDropping >= 350 && (xChange <= -200 || xChange > 250)){
                xChange -= 50;
            }
            if (yDropping >= 350 && (xChange > -100 && xChange <= 150 )){
                if (display[rowNumber][columnNumber - 1] == empty){
                    xChange -= 50;
                }
            }      
        }
        if ($hape == 4){
            if (yDropping < 300){
                xChange -= 50;
            }
            if (yDropping >= 300 && (xChange <= -150 || xChange > 250)){
                xChange -= 50;
            }
            if (yDropping >= 300 && (xChange > -100 && xChange <= 200 )){
                if (display[rowNumber][columnNumber - 1] == empty && display[rowNumber + 1][columnNumber - 1] == empty){
                    xChange -= 50;
                }
            }      
        }

    }
    if(keyEvent === 39){
        if ($hape == 1){
            if (yDropping < 350){
                xChange += 50;
            }
            if (yDropping >= 350 && (xChange < -150 || xChange >= 250)){
                xChange += 50;
            }
            if (yDropping >= 350 && (xChange >= -100 && xChange < 200 )){
                if (display[rowNumber][columnNumber + 1] == empty){
                    xChange += 50;
                }
            }      
        }
        if ($hape == 2){
            if (yDropping < 300){
                xChange += 50;
            }
            if (yDropping >= 300 && (xChange < -200 || xChange >= 250)){
                xChange += 50;
            }
            if (yDropping >= 300 && (xChange >= -100 && xChange < 150 )){
                if (display[rowNumber][columnNumber + 2] == empty && display[rowNumber + 1][columnNumber + 2] == empty ){
                    xChange += 50;
                }
            }      
        }
        if ($hape == 3){
            if (yDropping < 350){
                xChange += 50;
            }
            if (yDropping >= 350 && (xChange < -200 || xChange >= 250)){
                xChange += 50;
            }
            if (yDropping >= 350 && (xChange >= -100 && xChange < 150 )){
                if (display[rowNumber][columnNumber + 2] == empty){
                    xChange += 50;
                }
            }      
        }
        if ($hape == 4){
            if (yDropping < 300){
                xChange += 50;
            }
            if (yDropping >= 300 && (xChange < -150 || xChange >= 250)){
                xChange += 50;
            }
            if (yDropping >= 300 && (xChange >= -100 && xChange < 200 )){
                if (display[rowNumber][columnNumber + 1] == empty && display[rowNumber + 1][columnNumber + 1] == empty){
                    xChange += 50;
                }
            }      
        }
    }
    if(keyEvent === 38 && ($hape == 3 || $hape == 4)){
        if ($hape == 3){
            $hape = 4;
            b = 3;
            rowNumber -= 1;
            gameCopy = document.querySelector("#game");
            game = copy;
            game += shapes[b];
            gameCopy.innerHTML = game;
        }
        else if ((xChange != 200 && xChange != -150) || yDropping < 300 ){
            $hape = 3;
            b = 2;
            rowNumber += 1;
            gameCopy = document.querySelector("#game");
            game = copy;
            game += shapes[b];
            gameCopy.innerHTML = game;
        }        
    }
    if(keyEvent === 40 && yDropping <= 650) {
        yDropping += 50;
        rowNumber -= 1;
    }
    let dropString = 'translate('+ xChange + ' ' + yDropping + ')';
    document.getElementById($hape).setAttribute("transform",dropString)
    columnNumber = 2 + (xChange / 50);
    console.log(display);
    console.log(rowNumber);
    console.log(columnNumber);
    if (tooLow()){
        clearInterval(dropping);
        yDropping = 0;
        xChange = 0;
        columnNumber = 2;
        b = Math.floor(3 * Math.random());
        gameCopy = document.querySelector("#game");
        $hape = b + 1;
        if($hape == 2 || $hape == 4){
            rowNumber = 13;
        }
        else{
            rowNumber = 14 ;
        }
        game = copy;
        game += shapes[b];
        gameCopy.innerHTML = game;
        dropping = setInterval(Drop, 1337);
    }
    if(!CanDrop()){
        AddBlock();
    }
});








/* 
Current issue: 1: when a new block just comes out, if presses arrow keys right away the program is messed up.
                problem ?: when a key is pressed, the Drop function hasn't start being used yet.
               2: how to have arrow down key work properly, so the blocks don't go through the bottom line.
               3: need to figure out how to clean an entire row when it's full.
               4: update score.
               5: block goes right needs to update the codes.
               6: designs an alert when block goes over the limit of well, shows game over. 

*/